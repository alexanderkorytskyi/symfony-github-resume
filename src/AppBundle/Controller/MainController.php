<?php

namespace AppBundle\Controller;

use AppBundle\Handler\GitHubResponseHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class MainController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('username', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 4]),
                ],
                'label' => 'GitHub Username',
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Generate',
                'attr' => ['class' => 'btn-green']
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $username = $form->getData()['username'];
            $result = $this->makeApiCall(GitHubResponseHandler::CALL_USER_SHOW, $username);

            if ($result instanceof \Error) {
                $form->get('username')->addError(new FormError($result->getMessage()));
            } else {
                return $this->redirectToRoute('resume', ['username' => $username]);
            }
        }

        return $this->render('main/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/resume/{username}", name="resume")
     */
    public function showResumeAction($username = null)
    {
        if (!$username) {
            $this->redirectToRoute('home');
        }

        $user = $this->makeApiCall(GitHubResponseHandler::CALL_USER_SHOW, $username);

        if ($user instanceof \Error) {
            return $this->render('main/resume.html.twig', [
                'error' => $user->getMessage()
            ]);
        }

        $repos = $this->makeApiCall(GitHubResponseHandler::CALL_USER_REPOS, $username);

        if ($repos instanceof \Error) {
            return $this->render('main/resume.html.twig', [
                'error' => $repos->getMessage()
            ]);
        }

        return $this->render('main/resume.html.twig', [
            'user' => $user,
            'repos' => $repos->data,
            'stats' => $repos->stats
        ]);
    }

    private function makeApiCall($call, $query)
    {
        $client = $this->container->get('github.client');

        try {
            $result = $client->api('user')->$call($query);
        } catch (\Exception $e) {
            $result = new \Error($e->getMessage(), $e->getCode());
        }

        $handler = new GitHubResponseHandler($call, $result);

        return $handler->handle();
    }
}
