<?php

namespace AppBundle\Handler;

class GitHubResponseHandler
{
    const CALL_USER_SHOW = 'show';
    const CALL_USER_REPOS = 'repositories';

    const ERROR_DEFAULT = 0;
    const ERROR_USER_NOT_EXISTS = 1;
    const ERROR_USER_NO_EMAIL = 2;
    const ERROR_UNKNOWN_CALL = 3;

    /**
     * @var string
     */
    protected $call;

    /**
     * @var array | \Error
     */
    protected $response;

    /**
     * @param string $call
     * @param array|\Error $response
     */
    public function __construct($call, $response)
    {
        $this->call = $call;
        $this->response = $response;
    }

    /**
     * Calculate languages usage through all user repos
     *
     * @return array
     */
    public function getLanguageUsage()
    {
        $languages = [];

        foreach ($this->response as $repo) {
            $repo = (object)$repo;
            $lang = !empty($repo->language) ? $repo->language : 'Other';
            if (!isset($languages[$lang])) {
                $languages[$lang] = $repo->size;
            } else {
                $languages[$lang] += $repo->size;
            }
        }

        $languages['total'] = array_sum($languages);
        arsort($languages);

        return $languages;
    }
    /**
     * Entry point to handle API response
     *
     * @return array|\Error|object
     */
    public function handle()
    {
        if ($this->call === self::CALL_USER_SHOW) {
            return $this->handleUserShow();
        } else if ($this->call === self::CALL_USER_REPOS) {
            return $this->handleRepositories();
        } else {
            return $this->error(self::ERROR_UNKNOWN_CALL);
        }
    }


    /**
     * @param int $error
     * @return string
     */
    public static function getErrorMessage($error)
    {
        $messages = [
            self::ERROR_DEFAULT => 'Some error occurred. Please, try again later.',
            self::ERROR_USER_NOT_EXISTS => 'This username does not exists on GitHub.',
            self::ERROR_USER_NO_EMAIL => 'This user has not opted in.',
            self::ERROR_UNKNOWN_CALL => 'Can not process your request.'
        ];

        return $messages[$error];
    }


    /**
     * Handles response for user->show call
     *
     * @return \Error|object
     */
    private function handleUserShow()
    {
        if ($this->response instanceof \Error) {
            return $this->error($this->response->getCode() === 404 ? self::ERROR_USER_NOT_EXISTS : self::ERROR_DEFAULT);
        } elseif (null === $this->response['email']) {
            return $this->error(self::ERROR_USER_NO_EMAIL);
        } else {
            return $this->response;
        }
    }

    /**
     * Handles response for user->repositories call
     *
     * @return \Error|object
     */
    private function handleRepositories()
    {
        if ($this->response instanceof \Error) {
            return $this->error(self::ERROR_DEFAULT);
        } else {
            return (object)[
                'data' => $this->response,
                'stats' => $this->getLanguageUsage()
            ];
        }
    }

    /**
     * Wrapper for error response from this Handler
     *
     * @param int $error
     * @return \Error
     */
    private function error($error = self::ERROR_DEFAULT)
    {
        return new \Error(self::getErrorMessage($error), $error);
    }
}