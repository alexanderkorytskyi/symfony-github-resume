# symfony-github-resume

Test project implementing GitHub API on Symfony Framework

### Notes

I used gitHub API implementation provided by KnpLabs [KnpLabs/php-github-api](https://github.com/KnpLabs/php-github-api/tree/1.7/doc), which have no error management. If I implemented full GitHub API I would create clear and scalable handler with GitHub errors parsing.
 But for current task ```try { ... } catch { ... }``` construction should be enough to keep application logic simple.
 
 Assets presented as css without minification and compression. This is because of time limit and focus on back-end, but in real project I prefer to use css-preprocessors and Webpack.

### Requirements

Please, use **php 7.2** to run his project.

### Installation

Clone repository and run

```composer install```

While configuring parameters.yml you can use all default values.

### Run

Open project directory and run the following command:

```php bin/console server:start```
